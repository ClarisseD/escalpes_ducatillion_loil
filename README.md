# EscAlpes

Escape game cartographie sur le thème des Alpes

## Authors

* **Ducatillion Clarisse**
* **Loil Amandine**

## Installation

* Créer le projet sur votre machine
```
* Créez le projet web vide
* Dans le répertoire du projet, l'ouvrir dans un terminal
    * git init
    * git remote add origin https://gitlab.com/ClarisseD/escalpes_ducatillion_loil.git
    * git pull origin master
```

* Restaurer la base de donnée

Veuillez restaurer la base de données à l'aide du fichier **escalpes** dans le dossier bdd. (Nous avons utilisé pgAdmin4)

```
Avec PgAdmin :
* Ouvrez l'application
* Connectez-vous au serveur localhost
* Ajoutez une base de données (new database) de nom: escalpes
* Clic droit sur la base 
* Restaurer (restore) avec le fichier escalpes
```

* Lancer le jeu

Tout d'abord lancez votre server MAMP ou autre. 
Ensuite, ouvrez le fichier page_accueil.html qui se trouve dans le dossier html.

Laissez vous guider tout au long de l'aventure.

Un fichier d'aide est aussi disponible.