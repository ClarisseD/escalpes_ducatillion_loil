var mapid = document.getElementById("mapid");
document.getElementById("enigme2").style.visibility="hidden";
document.getElementById("enigme3").style.visibility="hidden";
document.getElementById("enigme4").style.visibility='hidden';
document.getElementById("enigme5").style.visibility='hidden';
document.getElementById("enigme6").style.visibility="hidden";
document.getElementById("enigme7").style.visibility="hidden";
document.getElementById("enigme8").style.visibility="hidden";
document.getElementById("reussite").style.visibility='hidden';
document.getElementById("echec").style.visibility="hidden";
var check = 1;

// chronometre
var sp = document.getElementsByTagName("span");
var btn_start=document.getElementById("start");
var btn_stop=document.getElementById("stop");
var t;
var ms=0,s=0,mn=0;
var penalite = 0;

t = setInterval(update_chrono,100);

function update_chrono(){
  ms+=1;
  /*si ms=10 <==> ms*cadence = 1000ms <==> 1s alors on incrémente le nombre de secondes*/
     if(ms==10){
      ms=1;
      s+=1;
     }
     /*on teste si s=60 pour incrémenter le nombre de minute*/
     if(s==60){
      s=0;
      mn+=1;
     }
     if(s>60){
       s=s-60;
       mn+=1;
     }
     if(penalite==1){
       s+=30;
       penalite=0;
     }
     if(mn==20){
      clearInterval(t);
      document.getElementById("enigme1").style.visibility="hidden";
      document.getElementById("enigme2").style.visibility="hidden";
      document.getElementById("enigme3").style.visibility="hidden";
      document.getElementById("enigme4").style.visibility='hidden';
      document.getElementById("enigme5").style.visibility='hidden';
      document.getElementById("enigme6").style.visibility="hidden";
      document.getElementById("enigme7").style.visibility="hidden";
      document.getElementById("enigme8").style.visibility="hidden";
      document.getElementById("reussite").style.visibility='hidden';
      document.getElementById("echec").style.visibility="visible";
      var min = document.getElementById("min").innerHTML;
      var sec = document.getElementById("sec").innerHTML;
      var temps = min + '.' + sec;
      var data = "time_joueur=" + temps + "&id=1";
      fetch('../php/jeu.php',{
        method:'post',
        body: data,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
     }
     /*afficher les nouvelle valeurs*/
     sp[0].innerHTML=mn;
     sp[1].innerHTML=s;
     sp[2].innerHTML=ms;

}

var audio = document.getElementById('audio');

audio.innerHTML = "<audio src='../mp3/abordage.mp3' autoplay='autoplay' preload='auto'></audio>";

var map = L.map(mapid).setView([45.499850, 6.736422], 15);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


var circleJeu = L.circle([45.505534, 6.710237], {
    opacity: 0.1,
    radius: 10010,
    color: 'black'
}).addTo(map);

var marker = L.marker([45.499850, 6.736422]).addTo(map)
  .bindPopup('Tout chamboulé, vous vous retrouvez ici !')
  .openPopup();
var popup2 = L.popup();

var enigme1 = document.getElementById("enigme1");
var valider1 = document.getElementById("valider1");
var reponse = document.getElementById("reponse");
var circle = L.circle([45.504539, 6.674259], {
    opacity: 0,
    radius: 5
}).addTo(map);
var circle1 = L.circle([45.511625, 6.697800], {
    opacity: 0,
    radius: 5
}).addTo(map);
  var ours = 0;
function Plan(e) {
  document.getElementById("plan").style.visibility='visible';
  ours = 1;

}
function Question2(e) {
  if (ours==1) {
    document.getElementById("enigme2").style.visibility='hidden';
    document.getElementById("plan").style.visibility='hidden';
    document.getElementById("question2").style.visibility='visible';
  }
}

function Validation1(e){
  if (document.getElementById("Bellecote").checked){
    reponse.innerHTML = "A = 45" + "<p><input classe='recup' id='inventaireA' type='submit' value='Récupérer'></p>";
    document.getElementById("inventaireA").addEventListener('click', Inventaire1);
  }
  else {
    reponse.innerHTML = "Mauvaise réponse ! Essayez encore...";
    penalite = 1;
  }
};

function Inventaire1(e) {
  document.getElementById('objet1').innerHTML = '<p>A = 45</p>';
  reponse.innerHTML = "<p><input classe='next' id='etape2' type='submit' value='Passer à Etape 2'></p>";
  document.getElementById("etape2").addEventListener('click', Etape2);
};

function Etape2(e) {
  document.getElementById('enigme1').style.visibility='hidden';
  document.getElementById("enigme2").style.visibility='visible';
  document.getElementById("question2").style.visibility='hidden';
  document.getElementById("plan").style.visibility='hidden';
  circle1.addEventListener('click', Plan);

  circle.addEventListener('click', Question2);
  check = 2;
}

valider1.addEventListener('click', Validation1);
var valider2 = document.getElementById("valider2");
var reponse2 = document.getElementById("reponse2");

function Validation2(e){

  if (document.getElementById("Ombre8").checked){
    reponse2.innerHTML = "B = 510" + "<p><input classe='recup' id='inventaireB' type='submit' value='Récupérer'></p>";
    document.getElementById("inventaireB").addEventListener('click', Inventaire2);
  }
  else {
    reponse2.innerHTML = "Mauvaise réponse ! Essayez encore...";
    penalite = 1;
  }
};

function Inventaire2(e) {
  document.getElementById('objet2').innerHTML = '<p>B = 510</p>';
  reponse2.innerHTML = "<p><input classe='next' id='etape3' type='submit' value='Passer à Etape 3'></p>";
  document.getElementById("etape3").addEventListener('click', Etape3);
};

function Etape3(e) {
  document.getElementById('enigme2').style.visibility='hidden';
  document.getElementById("question2").style.visibility='hidden';
  document.getElementById('enigme3').style.visibility='visible';
  document.getElementById('question3').style.visibility='hidden';

  check = 3;
}



valider2.addEventListener('click', Validation2);
var valider3 = document.getElementById("valider3");
var reponse3 = document.getElementById("reponse3");

var valeur1 = document.getElementById("valeur1");
var valeur2 = document.getElementById("valeur2");
var valeur3 = document.getElementById("valeur3");
var valeur4 = document.getElementById("valeur4");

var circle2 = L.circle([45.49254, 6.736808], {
    opacity: 0,
    radius: 5
}).addTo(map);

circle2.addEventListener('click', Question3);

function Question3(e){
  if (check == 3){
    document.getElementById("question3").style.visibility='visible';
  }

}

function Validation3(e){
  if (valeur1.value == 1 && valeur2.value == 9 && valeur3.value == 8 && valeur4.value == 3){
    reponse3.innerHTML = "C = 640" + "<p><input classe=recup' id='inventaireC' type='submit' value='Récupérer'></p>";
    document.getElementById("inventaireC").addEventListener('click', Inventaire3);
  }
  else {
    reponse3.innerHTML = "Mauvaise réponse ! Essayez encore...";
    penalite = 1;
  }
};

function Inventaire3(e) {
  document.getElementById('objet3').innerHTML = '<p>C = 640</p>';
  reponse3.innerHTML = "<p><input classe='next' id='etape4' type='submit' value='Passer à Etape 4'></p>";
  document.getElementById("etape4").addEventListener('click', Etape4);
};

function Etape4(e) {
  document.getElementById('enigme3').style.visibility='hidden';
  document.getElementById("question3").style.visibility='hidden';
  document.getElementById('enigme4').style.visibility='visible';
  document.getElementById("question4").style.visibility='hidden';
  audio.innerHTML = "<audio src='../mp3/abordage.mp3' autoplay='autoplay' preload='auto'></audio>";
  check = 4;
}



valider3.addEventListener('click', Validation3);

var circle3 = L.circle([45.450844, 6.67782], {
    opacity: 0,
    radius: 5
}).addTo(map);

circle3.addEventListener('click', Question4);

function Question4(e){
  if (check==4){
    document.getElementById("question4").style.visibility='visible';
  }

}
var reponse4 = document.getElementById("reponse4");
var valider4 = document.getElementById("valider4");
function Validation4(e){
  if(document.getElementById("issue1").checked){
    reponse4.innerHTML = "D = 6" + "<p><input classe='recup' id='inventaireD' type='submit' value='Récupérer'></p>";
    document.getElementById("inventaireD").addEventListener('click', Inventaire4);
  }
  else {
    reponse4.innerHTML = "Mauvaise réponse ! Essayez encore...";
    penalite = 1;
  }
};
function Inventaire4(e) {
  document.getElementById('objet4').innerHTML = '<p>D = 6</p>';
  reponse4.innerHTML = "<p><input classe='next' id='etape5' type='submit' value='Passer à Etape 5'></p>";
  document.getElementById("etape5").addEventListener('click', Etape5);
};
function Etape5(e){
  document.getElementById('enigme4').style.visibility='hidden';
  document.getElementById("question4").style.visibility='hidden';
  document.getElementById('enigme5').style.visibility='visible';
  document.getElementById("consigne1").style.visibility="hidden";
  check = 5;
};

valider4.addEventListener('click', Validation4);

var circle4 = L.circle([45.491081, 6.631274], {
    opacity: 0,
    radius: 5
}).addTo(map);

circle4.addEventListener('click', Question5);

function Question5(e){
  if (check==5){
    document.getElementById("consigne1").style.visibility = "visible";
  }

}
var reponse5 = document.getElementById("reponse5");
var valider5 = document.getElementById("valider5");

function Validation5(e){
  if (chiffre1.value == 2 && chiffre2.value == 3 && chiffre3.value == 5 && chiffre4.value == 0){
    reponse5.innerHTML = " La porte s'ouvre ! Quelqu'un vient vers vous et vous dit : E = 725" + "<p><input classe='recup' id='inventaireE' type='submit' value='Récupérer'></p>";
    document.getElementById("inventaireE").addEventListener('click', Inventaire5);
  }
  else {
    reponse5.innerHTML = "Mauvaise réponse ! Essayez encore...";
    penalite = 1;
  }
};
function Inventaire5(e){
  document.getElementById('objet5').innerHTML = '<p>E = 725</p>';
  reponse5.innerHTML = "<p><input classe='next' id='etape6' type='submit' value='Passer à Etape 6'></p>";
  document.getElementById("etape6").addEventListener('click', Etape6);
}

function Etape6(e){
  document.getElementById('enigme5').style.visibility='hidden';
  document.getElementById("consigne1").style.visibility="hidden";
  document.getElementById("enigme6").style.visibility='visible';
  check = 6;
}
valider5.addEventListener("click", Validation5);

var circle5 = L.circle([45.506166, 6.674451], {
    opacity: 0,
    radius: 5
}).addTo(map);

circle5.addEventListener('click', Question6);

function Question6(e){
  if (check==6){
    document.getElementById("consigne2").innerHTML = "Malheureusement ce DVA n'a plus de pile... Vous en trouverez peut-être au centre commercial de Aime - La Plagne";
    check = 7;
    audio.innerHTML = " <audio src='../mp3/bataille.mp3' autoplay='autoplay' preload='auto'></audio>";
  }
  if (check==8) {
    document.getElementById("consigne2").innerHTML = "Bravo le DVA fonctionne maintenant ! Il affiche : F = 017" + "<p><input classe='recup' id='inventaireF' type='submit' value='Récupérer'></p>" + "<p>Récupérer aussi le DVA, il pourra vous être utile pour localiser votre ami.</p>" + "<p><input id='dva' type='submit' value='Récupérer'></p>";
    document.getElementById("inventaireF").addEventListener('click', Inventaire6);
    document.getElementById("dva").addEventListener('click', Inventaire7);
  }
}
function Inventaire6(e){
  document.getElementById('objet6').innerHTML = '<p>F = 017</p>';
}
function Inventaire7(e){
  document.getElementById('objet7').innerHTML = "<p id='DVA'><img src='../img/DVA.jpg' height=50px</p>";
  reponse6.innerHTML = "<p><input classe='next' id='etape7' type='submit' value='Passer à Etape 7'></p>";
  document.getElementById("etape7").addEventListener('click', Etape7);

}

function Etape7(e){
  document.getElementById("enigme6").style.visibility="hidden";
  document.getElementById("enigme7").style.visibility='visible';
  check = 9
}

var circle6 = L.circle([45.552826, 6.647394], {
    opacity: 0,
    radius: 5
}).addTo(map);


circle6.addEventListener('click', Question7);

function Question7(e){
  if (check==7) {
    document.getElementById('consigne2').innerHTML = "Vous avez trouvé les piles ! Vite, allez récupérer votre DVA !";
    check = 8;
    document.getElementById('objet6').innerHTML = '<p><img src="../img/Pile.jpg" height=50px</p>';
  }
}

var circlePelle = L.circle([45.510640, 6.725017], {
    opacity: 0,
    radius: 5
}).addTo(map);

circlePelle.addEventListener('click', RecupPelle);

function RecupPelle(e){
  if (check==9) {
    document.getElementById('objet8').innerHTML = "<p id= 'pelle'><img src='../img/Pelle.jpg' height=50px</p>";
    audio.innerHTML = "<audio src='../mp3/fin.mp3' autoplay='autoplay' preload='auto'></audio>";
    document.getElementById("enigme7").style.visibility='hidden';
    document.getElementById("enigme8").style.visibility='visible';
    var circleZone = L.circle([45.499850, 6.736422], {
        opacity: 0.2,
        radius: 500,
        color: 'red'
    }).addTo(map);
    var dva = document.getElementById('DVA');
    var pelle = document.getElementById('pelle');

    dva.addEventListener("mousedown", function(e){
      var circleCroix = L.circle([45.498827, 6.733271], {
            color: 'red',
            opacity: 0,
            radius: 5
        }).addTo(map);
      circleCroix.addEventListener('mousemove', function(e){
          var Icon = L.Icon.extend({
            options: {
              iconSize: [32, 32],
              iconAnchor: [16,16],
              popupAnchor: [-3, -44]
            }

          });
          var iconCroix = new Icon({iconUrl: '../img/Croix.jpg'});
          var markerCroix = L.marker([45.498827, 6.733271], {icon: iconCroix}).addTo(map);
          circleCroix.removeEventListener('mousemove', function(e){});
          pelle.addEventListener("mousedown", function(e){
            markerCroix.addEventListener('mousemove', function(e){
              clearInterval(t);
              document.getElementById("enigme8").style.visibility='hidden';
              document.getElementById("reussite").style.visibility='visible';
              var min = document.getElementById("min").innerHTML;
              var sec = document.getElementById("sec").innerHTML;
              var temps = min + '.' + sec;
              var data = "time_joueur=" + temps + "&id=1";
              fetch('../php/jeu.php',{
                method:'post',
                body: data,
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                }
              })
            })
          })
        })
    })
  }
}

var coords = document.getElementById('coords');
function move (event){
  coords.innerHTML = "Latitude : " + event.latlng.lat + " Longitude : " + event.latlng.lng;
 }
map.on('mousemove', move);
coords.style.display = 'block';
