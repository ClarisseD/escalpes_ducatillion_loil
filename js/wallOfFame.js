document.getElementById('formJoueur').style.visibility = 'visible';
document.getElementById('classement').style.visibility = 'hidden';
document.getElementById('apresTop10').style.visibility = 'hidden';

var formulaire = document.getElementById("formJoueur");

var champ_nom = formulaire.elements["nom"];
var champ_prenom = formulaire.elements["prenom"];

function valider(event){
  event.preventDefault();

  var champ_nom = formulaire.elements["nom"];
  var champ_prenom = formulaire.elements["prenom"];

  // le formulaire est-il OK?
  var form_OK = true;

  if(champ_nom.value == ""){
    form_OK = false;
    champ_nom.classList.add("erreur");
  }else{
    champ_nom.classList.remove("erreur");
  }

  if(champ_prenom.value == ""){
    form_OK = false;
    champ_prenom.classList.add("erreur");
  }else{
    champ_prenom.classList.remove("erreur");
  }

  // Au final, on empeche l'envoi du formulaire si form_OK est faux
  if(!form_OK){
    event.preventDefault();
  } else{
    toBDD(champ_nom.value, champ_prenom.value);
  }

}

formulaire.addEventListener('submit', valider);

var joueur = [];
joueur[0] = document.getElementById('joueurN1');
joueur[1] = document.getElementById('joueurN2');
joueur[2] = document.getElementById('joueurN3');
joueur[3] = document.getElementById('joueurN4');
joueur[4] = document.getElementById('joueurN5');
joueur[5] = document.getElementById('joueurN6');
joueur[6] = document.getElementById('joueurN7');
joueur[7] = document.getElementById('joueurN8');
joueur[8] = document.getElementById('joueurN9');
joueur[9] = document.getElementById('joueurN10');


function toBDD(champ_nom, champ_prenom){
  document.getElementById('formJoueur').style.visibility = 'hidden';
  document.getElementById('classement').style.visibility = 'visible';
  document.getElementById('apresTop10').style.visibility = 'hidden';

  var temps = 0;

  fetch('../php/recupTemps.php',{
    method: 'post',
    body: 'toto',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  .then(r => r.json())
  .then(r => {
    temps = r[0];

    var data = "nom=" + champ_nom + "&prenom=" + champ_prenom + "&temps=" + temps;
    var dedans = 0;
    var joueurPartie = champ_nom + ' ' + champ_prenom + ' ' + temps;
    fetch('../php/wallOfFame.php', {
      method: 'post',
      body: data,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
    .then(r => r.json())
    .then(r2 => {
        for (var i = 0; i<r2.length; i++){
        joueur[i].innerHTML = i+1 + ' ' + r2[i];
        var num = i+1;
        var numJoueur = 'joueurN' + num;
        if (document.getElementById(numJoueur).innerHTML == i+1 + ' ' + joueurPartie){ // essayer de récupérer ce qu'il y a dans le li qui a l'id numJoueur
          dedans = 1;
        }
        if (dedans == 0){
          document.getElementById('apresTop10').style.visibility = 'visible';
          document.getElementById('joueurPartie').innerHTML = joueurPartie;
        }
      }
    })
  })



}
