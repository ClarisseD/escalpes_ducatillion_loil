<?php
    $conn_string = "host=localhost port=5432 dbname=escalpes user=postgres password=postgres";
    $db_connection = pg_connect($conn_string);

    pg_insert($db_connection, 'wall_of_fame', $_POST);

    $recup_joueur = pg_query($db_connection, "SELECT nom, prenom, temps FROM wall_of_fame ORDER BY temps LIMIT 10");

      if($recup_joueur){
        $joueur = [];
        while($meilleur_joueur = pg_fetch_array($recup_joueur)){
          $joueur[] = trim($meilleur_joueur["nom"]) . ' ' . trim($meilleur_joueur["prenom"]) . ' ' . trim($meilleur_joueur["temps"]);
        }
      }
      echo json_encode($joueur);

?>
